﻿#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <poll.h>
#include <sys/utsname.h>
#include <sys/time.h>
#include <signal.h>

int service_loop(int argc, char** argv)
{
    int i;
    char* child_argv[8] = {0};
    pid_t pid;
    int status = 100;
    int ret;
    for (i = 1; i < argc; ++i)
    {
        child_argv[i - 1] = (char*)malloc(strlen(argv[i]) + 1);
        strncpy(child_argv[i - 1], argv[i], strlen(argv[i]));
        child_argv[i - 1][strlen(argv[i])] = 0;
    }
    while (1)
    {
        pid = fork();
        if (pid == -1)
        {
            continue;
        }
        if (pid == 0)
        {
            pid_t child_pid = getpid();
            printf("child process %s(%d)", child_argv[0], child_pid);
            ret = execv(child_argv[0], (char**)child_argv);
            if (ret < 0)
            {
                continue;
            }
        }
        if (pid > 0)
        {
            pid = wait(&status);
        }
    }
}

int main(int argc, char** argv)
{
    int i;
    pid_t pid_main;
    if (argv[0] == NULL)
        return -1;
    if (strlen(argv[0]) == 0)
        return -1;
    pid_main = fork();
    if (pid_main == -1)
        return -1;
    else if (pid_main == 0)
    {
        //child process.
        pid_t child_pid = getpid();
        service_loop(argc, argv);
    }
    else if (pid_main > 0)
    {
        return 0;
    }
}

